import React from "react";
import style from "../styles/Style.css";
import QuoteText from "../../models/QuoteText";

const Quote = (props) => {
  return (
    props.attachment ? (
      <div className={style.blockquoteWrapper}>
        <div>{props.attachment.leftText}</div>
        <div className={style.quote}>{props.attachment.rightText}</div>
      </div>
    ) : (
      <span />
    )
  );
};

Quote.propTypes = {
  attachment: QuoteText.isRequired,
};
  
export default Quote;
