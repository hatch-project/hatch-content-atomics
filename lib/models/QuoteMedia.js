import PropTypes from "prop-types";

export default PropTypes.shape({
  text: PropTypes.string.isRequired,
  media: PropTypes.string.isRequired,
  webLink: PropTypes.string.isRequired,
  source: PropTypes.string,
  sourceLink: PropTypes.string,
});
