import PropTypes from "prop-types";

export default PropTypes.shape({
  media: PropTypes.string.isRequired,
  webLink: PropTypes.string.isRequired,
});
