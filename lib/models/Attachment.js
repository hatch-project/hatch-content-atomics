import PropTypes from "prop-types";
import QuoteText from "./QuoteText";
import QuoteMedia from "./QuoteMedia";
import Gallery from "./Gallery";

export default PropTypes.shape({
  attachmentType: PropTypes.string.isRequired,
  frontPageOrder: PropTypes.number.isRequired,
  quoteText: QuoteText,
  quoteMedia: QuoteMedia,
  gallery: PropTypes.arrayOf(Gallery),
});
