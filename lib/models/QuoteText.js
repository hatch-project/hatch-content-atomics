import PropTypes from "prop-types";

export default PropTypes.shape({
  leftText: PropTypes.string.isRequired,
  rightText: PropTypes.string.isRequired,
});
