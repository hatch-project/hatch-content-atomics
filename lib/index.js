import QuoteText from "./quoteText/components/Quote";
import QuoteMedia from "./quoteMedia/components/Quote";
import Gallery from "./gallery/components/Gallery";
import Video from "./video/components/Video";
import Attachment from "./models/Attachment";

module.exports = {
  QuoteText,
  QuoteMedia,
  Gallery,
  Video,
  Attachment,
};
