import React from "react";
import PropTypes from "prop-types";
import style from "../styles/Style.css";
import GalleryModel from "../../models/Gallery";
import Lightbox from "react-images";

class Gallery extends React.Component {
  constructor(props) {
    super(props);

    // Map images array and copy it
    const propsImages = (this.props.attachment ? this.props.attachment : []).map(it => { 
      return { 
        src: it.webLink,
        caption: it.source,
      };
    });
    const images = propsImages.slice().slice(0, this.props.maxImages);

    // Thumbnails dependending on the number of images
    const thumbnailSizeSingle = {
      width: ["100%"],
      height: ["250px"],
      backgroundSize: "contain",
    };
    const thumbnailSizeShort = {
      width: ["60%", "38%"],
      height: ["510px", "250px"],
      backgroundSize: "cover",
    };
    const thumbnailSizeBig = {
      width: ["50%", "22%"],
      height: ["310px", "150px"],
      backgroundSize: "cover",
    };
    const thumbnailSize = images.length <= 3 ? (images.length === 1 ? thumbnailSizeSingle : thumbnailSizeShort) : thumbnailSizeBig;
    const numberOfimagesHidden = propsImages.length <= this.props.maxImages ? 0 : propsImages.length - this.props.maxImages;

    const style = images.map((element, key) => {
      return {
        cursor: "pointer",
        boxSizing: "border-box",
        display: "block",
        backgroundImage: `url(${element.src})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: thumbnailSize.backgroundSize,
        backgroundPosition: "center center",
        marginBottom: "10px",
        marginLeft: key === 0 ? "auto" : "1%",
        marginRight: key === 0 ? "auto" : "1%",
        float: key === 0 && images.length === 1 ? "none" : "left",
        width: key === 0 ? thumbnailSize.width[0] : thumbnailSize.width[1],
        height: key === 0 ? thumbnailSize.height[0] : thumbnailSize.height[1],
        opacity: key === (this.props.maxImages - 1) && numberOfimagesHidden > 0 ? 0.5 : 1,
      };
    });

    this.state = {
      lightboxIsOpen: false,
      currentImage: 0,
      propsImages: propsImages,
      images: images,
      numberOfimagesHidden: numberOfimagesHidden,
      style: style,
    };

    this.handleClose = this.handleClose.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handlePrevious = this.handlePrevious.bind(this);
    this.handleSetImage = this.handleSetImage.bind(this);
    this.handleClickImage = this.handleClickImage.bind(this);
    this.openLightbox = this.openLightbox.bind(this);
  }

  openLightbox (index, event) {
    event.preventDefault();
    this.setState({
      currentImage: index,
      lightboxIsOpen: true,
    });
  }

  handleClose () {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  }

  handlePrevious () {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  }

  handleNext () {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  }

  handleSetImage (index) {
    this.setState({
      currentImage: index,
    });
  }

  handleClickImage () {
    if (this.state.currentImage === this.state.images.length - 1) {
      return;
    }

    this.handleNext();
  }

  render() {

    return (
      <div>
        <div>
          {this.state.style.map((element, key) => {
            return (
              <div 
                key={key}
                onClick={(event) => this.openLightbox(key, event)}
                style={element}
              >
                {key === (this.props.maxImages - 1) && this.state.numberOfimagesHidden > 0 &&
                  <p className={style.hiddenImages}>+ {this.state.numberOfimagesHidden}</p>
                }
              </div>
            );
          })
          }
        </div>
        <div className={style.clear} />
        <Lightbox
          currentImage={this.state.currentImage}
          images={this.state.propsImages}
          isOpen={this.state.lightboxIsOpen}
          onClickImage={this.handleClickImage}
          onClickNext={this.handleNext}
          onClickPrev={this.handlePrevious}
          onClickThumbnail={this.handleSetImage}
          onClose={this.handleClose}
          showThumbnails
        />
      </div>
    );
  }
}

Gallery.propTypes = {
  attachment: PropTypes.arrayOf(GalleryModel),
  maxImages: PropTypes.number,
};

Gallery.defaultProps = {
  maxImages: 5,
};
  
export default Gallery;
