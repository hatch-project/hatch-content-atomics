import React from "react";
import style from "../styles/Style.css";
import QuoteMedia from "../../models/QuoteMedia";

const Quote = (props) => {
  return (
    props.attachment ? (
      <div className={style.blockquoteWrapper}>
        <div className={style.quote}>{props.attachment.text}</div>
        <div>
          {props.attachment.media === "image" &&
            <div className={style.alignRight}>
              <img
                alt={props.attachment.source}
                src={props.attachment.webLink}
              />
              <span className={style.source}>{props.attachment.source}</span>
            </div>
          }
        </div>
      </div>
    ) : (
      <span />
    )
  );
};

Quote.propTypes = {
  attachment: QuoteMedia.isRequired,
};

export default Quote;
