import React from "react";
import style from "../styles/Style.css";
import Video from "../../models/Video";
import ReactPlayer from "react-player";

const VideoComponent = (props) => {
  return (
    props.attachment ? (
      <div className={style.root}>
        {(props.attachment.media === "video" || props.attachment.media === "youtube") &&
          <ReactPlayer
            controls
            height="360px"
            muted={false}
            playing={false}
            url={props.attachment.webLink}
            width="100%"
          />
        }
      </div>
    ) : (
      <span />
    )
  );
};

VideoComponent.propTypes = {
  attachment: Video.isRequired,
};

export default VideoComponent;
