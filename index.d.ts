// Type definitions for hatch-content-atomics > v0.1.1
// Project: https://bitbucket.org/hatch-project/hatch-content-atomics

import * as React from "react";

declare class QuoteText extends React.Component<QuoteText.Props, any> {}

declare namespace QuoteText {

  export interface Props {
    attachment: {
      leftText: string
      rightText: string
    }
  }
}

declare class QuoteMedia extends React.Component<QuoteMedia.Props, any> {}

declare namespace QuoteMedia {

  export interface Props {
    attachment: {
      text: string,
      media: string,
      webLink: string,
      source?: string,
      sourceLink?: string,
    }
  }
}

declare class Gallery extends React.Component<Gallery.Props, any> {}

declare namespace Gallery {

  export interface Props {
    attachment: Array<{
      media: string,
      webLink: string,
      source?: string,
      sourceLink?: string,
    }>,
  }
}

declare class Video extends React.Component<Video.Props, any> {}

declare namespace Video {

  export interface Props {
    attachment: {
      media: string,
      webLink: string,
    }
  }
}